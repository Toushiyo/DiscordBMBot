from liblol import RiotWebAPI
from os import getenv
from dotenv import load_dotenv

import discord
import platform
import datetime

environment_os = platform.system()

"""
ENV CONSTANTS
"""
""" ENV CONSTANTS """
if environment_os is "Windows":
    load_dotenv()

RIOT_API_KEY = getenv("RIOT_API_KEY")
DISCORD_BOT_KEY = getenv("DISCORD_BOT_KEY")

client = discord.Client()
lol_web_api = RiotWebAPI(api_key=RIOT_API_KEY, verbose=True)


@client.event
async def on_ready():
    print("We have logged in as {0.user}".format(client))


@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith("$hello"):
        await message.channel.send("Hello!")

    elif message.content.startswith("$player"):
        await message.channel.send("Hello Player!")


@client.event
async def on_member_update(before: discord.Member, after: discord.Member):
    user = before.display_name
    event_time = datetime.datetime.now()

    print(
        f"[{event_time.strftime('%H:%M:%S')}] {user} switched from {before.activity} -> {after.activity}"
    )


client.run(DISCORD_BOT_KEY)
