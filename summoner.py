from liblol import RiotWebAPI

class Summoner:

    # Initalize obj and get data from Riot's Api using summoner name
    def __init__(self, name):
        self.summonerName = name
        # Get summoner info from RiotWebApi
        summonerInfo = lol_web_api.query_summoner_info(self.summonerName)
        self.summonerId = summonerInfo["summonerId"]
        self.tier = summonerInfo["tier"]
        self.rank = summonerInfo["rank"]
        self.leaguePoints = summonerInfo["leaguePoints"]
        self.hotStreak = summonerInfo["hotStreak"]

    def updateUser(self):
        # Get summoner info from RiotWebApi
        summonerInfo = lol_web_api.query_summoner_info(self.summonerName)
        self.tier = summonerInfo["tier"]
        self.rank = summonerInfo["rank"]
        self.leaguePoints = summonerInfo["leaguePoints"]
        self.hotStreak = summonerInfo["hotStreak"]
        
    def __test__(self):
        print("Running summoner class: __test__ method")
        pass

if __name__ == "__main__":
    from os import getenv
    from dotenv import load_dotenv
    import platform

    if platform.system() == "Windows":
        load_dotenv()
        RIOT_API_KEY = getenv("RIOT_API_KEY")

    lol_web_api = RiotWebAPI(api_key=RIOT_API_KEY, verbose=True)
    summoner = Summoner(name="JhinRummy")
    summoner.__test__()